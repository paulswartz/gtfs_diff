from gtfs_diff import Diff


def diff(old, new):
    return Diff.from_df_with_key(old.routes, new.routes, "route_id")
