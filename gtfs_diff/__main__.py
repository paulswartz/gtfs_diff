import argparse
import gtfs_diff

parser = argparse.ArgumentParser()
parser.add_argument("old_gtfs")
parser.add_argument("new_gtfs")
args = parser.parse_args()
diff = gtfs_diff.from_files(args.old_gtfs, args.new_gtfs)
print(gtfs_diff.feed_from_diff(diff))
