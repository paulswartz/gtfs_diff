import gtfs_diff.feed
import attr
import gtfs_kit
import pandas as pd


@attr.s
class FeedDiff:
    old_gtfs = attr.ib()
    new_gtfs = attr.ib()
    routes = attr.ib()
    stops = attr.ib()
    trips = attr.ib()

    @classmethod
    def from_feeds(klass, old_gtfs, new_gtfs):
        from gtfs_diff import tables

        diff = {}
        for table in ["stops", "trips", "routes"]:
            d = getattr(tables, table).diff(old_gtfs, new_gtfs)
            if d is None:
                d = Diff()
            diff[table] = d

        return klass(
            old_gtfs=old_gtfs,
            new_gtfs=new_gtfs,
            stops=diff["stops"],
            routes=diff["routes"],
            trips=diff["trips"],
        )


@attr.s
class Diff:
    added = attr.ib(default=None)
    changed = attr.ib(default=None)
    removed = attr.ib(default=None)

    @classmethod
    def from_df_with_key(klass, old, new, key):
        old = old.set_index(key)
        old.sort_index(0, inplace=True)
        new = new.set_index(key)
        new.sort_index(0, inplace=True)
        merged = new.merge(
            old,
            indicator=True,
            how="outer",
            left_index=True,
            right_index=True,
            suffixes=("", "_old"),
        )
        added_index = merged._merge.eq("left_only")
        removed_index = merged._merge.eq("right_only")
        same_index = merged._merge.eq("both")
        added = None
        changed = None
        removed = None
        if added_index.any():
            added = new.loc[added_index]
            added.reset_index(key, inplace=True)
        if removed_index.any():
            removed = old.loc[removed_index]
            removed.reset_index(key, inplace=True)
        if same_index.any():
            # TODO can we do some comparison of the fields in `merged`?
            to_keep = []
            changed = new.loc[same_index]
            for index in changed.index:
                if not new.loc[index].equals(old.loc[index]):
                    to_keep.append(index)
            if to_keep:
                changed = changed.loc[to_keep]
                changed.reset_index(key, inplace=True)
            else:
                changed = None

        if added is None and changed is None and removed is None:
            return None

        return klass(added=added, changed=changed, removed=removed)


def from_files(old, new):
    old = gtfs_kit.read_feed(old, dist_units="m")
    new = gtfs_kit.read_feed(new, dist_units="m")
    return FeedDiff.from_feeds(old, new)


def feed_from_diff(diff):
    return feed.from_diff(diff)
