"""
Generates a GTFS-RT feed from a Diff.
"""
import gtfs_diff.gtfs_realtime_pb2 as pb
import gtfs_kit
import pytz
import pandas
import time
import datetime


def from_diff(diff):
    entities = new_routes(diff) + new_trips(diff)
    return pb.FeedMessage(header=feed_header(), entity=entities)


def feed_header():
    return pb.FeedHeader(
        gtfs_realtime_version="2.0",
        incrementality=pb.FeedHeader.Incrementality.FULL_DATASET,
        timestamp=int(time.time()),
    )


def new_routes(diff):
    new_routes = diff.routes.added
    if new_routes is None:
        return []
    return [new_route(r) for r in new_routes.itertuples()]


def new_route(r):
    return pb.FeedEntity(
        route=pb.Route(
            route_id=r.route_id,
            agency_id=r.agency_id,
            route_short_name=translated_string(r.route_short_name),
            route_long_name=translated_string(r.route_long_name),
            route_desc=translated_string(r.route_desc),
            route_type=r.route_type,
            route_url=translated_string(r.route_url),
            route_color=nan_default(r.route_color, None),
            route_text_color=nan_default(r.route_text_color, None),
            route_sort_order=nan_default(r.route_sort_order, None),
        )
    )


def new_trips(diff):
    new_trips = diff.trips.added
    if new_trips is None:
        return []
    today = gtfs_kit.datestr_to_date(datetime.date.today(), inverse=True)
    dates = [d for d in diff.new_gtfs.get_dates() if d >= today]
    return [new_trip(diff, dates, t) for t in new_trips.itertuples()]


def new_trip(diff, dates, t):
    return pb.FeedEntity(
        trip=pb.Trip(
            trip_id=t.trip_id,
            route_id=nan_default(t.route_id, None),
            trip_headsign=translated_string(t.trip_headsign),
            trip_short_name=nan_default(t.trip_short_name, ""),
            direction_id=nan_default(t.direction_id, None),
            block_id=nan_default(t.block_id, None),
            shape_id=nan_default(t.shape_id, None),
            wheelchair_accessible=nan_default(t.wheelchair_accessible, 0),
            biked_allowed=nan_default(t.bikes_allowed, 0),
            stop_times=new_stop_times(diff, dates, t),
        )
    )


def new_stop_times(diff, dates, t):
    dates = [d for d in dates if diff.new_gtfs.is_active_trip(t.trip_id, d)]
    stop_times = diff.new_gtfs.stop_times
    stop_times = stop_times.loc[stop_times.trip_id.eq(t.trip_id)]
    return [
        pb.StopTime(
            stop_sequence=s.stop_sequence,
            arrival_time=epoch_from_date_time(d, s.arrival_time),
            departure_time=epoch_from_date_time(d, s.departure_time),
            stop_id=s.stop_id,
            stop_headsign=translated_string(s.stop_headsign),
            pickup_type=nan_default(s.pickup_type, pb.StopTimeProperties.PickupType.REGULAR_PICKUP),
            drop_off_type=nan_default(
                s.drop_off_type, pb.StopTimeProperties.DropOffType.REGULAR_DROP_OFF
            ),
        )
        for d in dates
        for s in stop_times.itertuples()
    ]


def translated_string(text):
    if pandas.isnull(text):
        return None

    return pb.TranslatedString(translation=[pb.TranslatedString.Translation(text=text)])


def nan_default(value, default):
    if pandas.isnull(value):
        return default

    return value


TWELVE_HOURS = 12 * 3600


def epoch_from_date_time(date_str, time_str, timezone_name="America/New_York"):
    """
    Returns an epoch timestamp, given a YYYYMMDD date string and an HH:MM:SS time.

    The time is relative to 12 hours before noon on the given date, and can be more than 24 hours.
    """
    date = gtfs_kit.datestr_to_date(date_str)
    naive_noon = datetime.datetime.combine(date, datetime.time(12))
    tzinfo = pytz.timezone(timezone_name)
    noon = tzinfo.localize(naive_noon)
    [hours, minutes, seconds] = [int(x) for x in time_str.split(":", 2)]
    seconds = hours * 3600 + minutes * 60 + seconds - TWELVE_HOURS
    dt = noon + datetime.timedelta(seconds=seconds)
    return int(dt.timestamp())
