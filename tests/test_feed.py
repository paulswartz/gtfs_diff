from gtfs_diff import feed


def test_epoch_from_date_time():
    assert feed.epoch_from_date_time("19700101", "0:00:00", "Etc/UTC") == 0
    assert feed.epoch_from_date_time("19700101", "0:00:00", "Etc/UTC") == 0
    assert feed.epoch_from_date_time("19691231", "24:00:01", "Etc/UTC") == 1
    assert feed.epoch_from_date_time("20200306", "02:05:33") == 1583478333
    assert feed.epoch_from_date_time("20200308", "13:44:49") == 1583689489
