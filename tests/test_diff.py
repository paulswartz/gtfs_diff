from gtfs_diff import Diff
import pandas as pd
import pytest


def test_diff_added():
    new = pd.DataFrame([{"id": 1, "value": 1}])
    old = pd.DataFrame([], columns=new.columns)

    diff = Diff.from_df_with_key(old, new, "id")
    print(diff.added)
    print(new)
    pd.testing.assert_frame_equal(diff.added, new)


def test_diff_removed():
    old = pd.DataFrame([{"id": 1, "value": 1}])
    new = pd.DataFrame([], columns=old.columns)

    diff = Diff.from_df_with_key(old, new, "id")
    pd.testing.assert_frame_equal(diff.removed, old)


def test_diff_changed():
    old = pd.DataFrame([{"id": 1, "value": 1}, {"id": 2, "value": 2}])
    new = pd.DataFrame([{"id": 1, "value": 2}, {"id": 2, "value": 2}])

    changed = pd.DataFrame([{"id": 1, "value": 2}])

    diff = Diff.from_df_with_key(old, new, "id")
    pd.testing.assert_frame_equal(diff.changed, changed)


def test_diff_all_three():
    old = pd.DataFrame([{"id": 0, "value": 0}, {"id": 1, "value": 1}, {"id": 2, "value": 2}])
    new = pd.DataFrame([{"id": 1, "value": 2}, {"id": 2, "value": 2}, {"id": 3, "value": 3}])

    added = pd.DataFrame([{"id": 3, "value": 3}])
    changed = pd.DataFrame([{"id": 1, "value": 2}])
    removed = pd.DataFrame([{"id": 0, "value": 0}])

    diff = Diff.from_df_with_key(old, new, "id")
    pd.testing.assert_frame_equal(diff.added, added)
    pd.testing.assert_frame_equal(diff.changed, changed)
    pd.testing.assert_frame_equal(diff.removed, removed)
